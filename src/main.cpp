#include <Arduino.h>
int motor1=7;
int motor2=8;


void TurnLeft(int time)
{
	delay(time*1000);
	digitalWrite(motor1, HIGH);
	digitalWrite(motor2, LOW);
}
void TurnRight(int time)
{
	delay(time*1000);
	digitalWrite(motor1, LOW);
	digitalWrite(motor2, HIGH);
}
void forward(int time)
{
	delay(time*1000);
	digitalWrite(motor1, HIGH);
	digitalWrite(motor2, HIGH);
}
void stop()
{
	digitalWrite(motor2, LOW);
	digitalWrite(motor1, LOW);
}
void setup()
{
	pinMode(motor1, OUTPUT);
	pinMode(motor2, OUTPUT);
	delay(100);
	forward(5);
	TurnLeft(1);
	forward(10);
	TurnRight(1);
}
void loop()
{

stop();

}
